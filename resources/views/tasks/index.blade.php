@extends('layouts.app')
@section('content')

<h1>This the tasks list </h1> 
@if (Request::is('tasks'))  

<h2><a href="{{action('TaskController@my_tasks')}}">My Tasks</a></h2>
@else
<h2><a href="{{action('TaskController@index')}}">All Tasks</a></h2>

@endif


<table class=table>
    <thead>  
        <tr>  
        <th>Title</th> 
        <th>Owner</th> 
        <th>Done</th> 
        <th>Status</th>
        <th>Edit</th>
        @can('admin')<th>Delete</th> @endcan
        </tr>  
    </thead>
    @foreach($tasks as $task)
        <tr> 
        <td>   {{$task->title}} </td>
        <td>   {{$task->user_id}}  </td>
        <td>    {{$task->status}}  </td>
         <td>    @if ($task->status)
                Done 
            @else @can('admin')
                  <a href="{{route('tasks.change_status', [$task -> id, $task -> status])}}" >Mark as done</a>
                 @endcan
                   </td> 
            @endif
        <td>  <a href = "{{route('tasks.edit',$task -> id)}}" >  Edit the task </a></td>
        
        @can('admin')
            <td> <form method = 'post' action = "{{action('TaskController@destroy', $task->id)}}">
            @csrf
            @method('DELETE')
            <div class="form-group">
                <input type="submit" class="form=control" name="submit" value="Delete">
            </div>

            </form></td>

       @endcan
        </tr>
    @endforeach
 </table> 
  <a  href="{{route('tasks.create')}}"> Create a new task </a> 


@endsection