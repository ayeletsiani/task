<?php

use Illuminate\Database\Seeder;

class TaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert([[
            'title' => 'test task 1',
            'created_at' =>date('Y-m-d G:i:s'),
            'user_id' => 1
            ],
            
            [
            'title' => 'test task 2',
            'created_at' =>date('Y-m-d G:i:s'),
            'user_id' => 1
            ],
            [
            'title' => 'test task 3',
            'created_at' =>date('Y-m-d G:i:s'),
            'user_id' => 1
            ],
        ]);

    }
}